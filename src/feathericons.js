import Icons from 'feather-icons';


const IconStyle = {'stroke-width':2, color:'#1EAEDB'};
const IconStyleSmall = {...IconStyle};
IconStyleSmall.width = 12;
IconStyleSmall.height = 12;
const IconStyleSmallWhite = {... IconStyleSmall};
IconStyleSmallWhite.color = "#FFFFFF";

const Icon = function(name,size)
{
  let style = IconStyle;

  if(typeof size !== 'undefined')
  {
    switch(size)
    {
      case "small":
        style = IconStyleSmall;
        break;
        case "small-white":
        style = IconStyleSmallWhite;
        break;
    }
  }

  return Icons.icons[name].toSvg(style);
}

export {Icon, IconStyle};
