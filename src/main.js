import SouthCamsMap from './PlacesMap.svelte';
import SouthCamsInfo from './SouthCamsInfo.svelte';

let southCamsComponents = []

const componentMap = [
	{
		"element":"SouthCamsPlaces",
		"object":SouthCamsMap
	},
	{
		"element":"SouthCamsInfo",
		"object":SouthCamsInfo
	},
]

for(const mapping of componentMap)
{
	let elements = document.getElementsByTagName(mapping["element"])
	for (let x=0;x< elements.length; x++)
	{
		elements[x].innerHTML = "";
		let a = new mapping["object"]({
			target: elements[x]
		});
		southCamsComponents.push(a);
	}
}

export default southCamsComponents;
