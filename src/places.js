import { writable } from 'svelte/store';

const selectedPlace = writable(null);


const Places = [
  {"label":"Whaddon","location":[52.1023,-0.0281]},
  {"label":"Harston","location":[52.13821,0.08502]},
  {"label":"Royston","location":[52.0481094661956,-0.02424716949462891]},
  {"label":"Therfield","location":[52.01707,-0.05326]},
  {"label":"Barrington","location":[52.12926,0.03870]},
  {"label":"Melbourn","location":[52.08397,0.01645]},
  {"label":"Shepreth","location":[52.11244,0.03308]},
  {"label":"Whittlesford","location":[52.11325243469633,0.14990034101903585]}
  //to add - Newton - need main pic, Thriplow - ready, Fowlmere - need main pic,  - ready
  //Wimpole, Meldreth

];

const PlaceDetails = {
  "Melbourn":{
    "zoom":14,
    "location":[52.0819,0.0163],
    "places":[
      {"label":"stockbridge_meadows","location":[52.08109,0.01042]},
      {"label":"the_moor","location":[52.08898658345996, 0.014349818229675293]},
      {"label":"melbourn_fields_playground","location":[52.08924697430052,0.017769634723663334]},
      {"label":"melbourn_enclosed_playground","location":[52.08033678877738,0.021712481975555423]}
    ]
  },
  "Harston":{
    "zoom":15,
    "location":[52.1369124294351,0.08170735744422954],
    "places":[
      {"label":"harston_rec","location":[52.13783,0.08518]}

    ]
  },
  "Royston":{
    "zoom":14,
    "location":[52.05106537537049,-0.025577545166015625],
    "places":[
      {"label":"the_heath","location":[52.04665,-0.03619]},
      {"label":"royston_splash_park", "location":[52.04671721802381,-0.02101242542266846]},
      {"label":"royston_rockets", "location":[52.05935803192541,-0.020974874496459964]}
    ]
  },
  "Therfield":{
    "zoom":16,
    "location":[52.01735113615258,-0.05533933639526368],
    "places":[{"label":"fox_and_duck","location":[52.01733793060974,-0.05424812874598395]}]
  },
  "Barrington":{
    "zoom":15,
    "location":[52.12653308753329,0.03536770726934258],
    "places":[
      {"label":"Toddler_Park","location":[52.129480513242136,0.0379130244255066]},
      {"label":"Big_Kid_Park","location":[52.12977030546622,0.041150450706481934]},
      {"label":"the_royal_oak","location": [52.12614118049174, 0.028823018074035648]}
    ]
  },
  "Shepreth":{
    "zoom":15,
    "location":[52.11204011455013,0.033089563140000784],
    "places":[
      {"label":"Shepreth_Playground","location":[52.11373,0.03398]},
      {"label":"Wildlife_Park","location":[52.11446,0.03611]}
    ]
  },
  "Whaddon":{
    "zoom":15,
    "location":[52.09928239995236,-0.02913539266514942],
    "places":[{"label":"Whaddon_Playground","location":[52.09969761030772,-0.03156960010528565]}]
  },
  "Whittlesford":{
    "zoom":15,
    "location":[52.11325243469633,0.14990034101903585],
    "places":[
      {"label":"whitlesford_playground","location":[52.11244861741238,0.15345350476438482]},
      {"label":"tickell_arms","location":[52.11346656390523,0.1485063089973382]}
    ]

  }
}

export {Places,PlaceDetails, selectedPlace};
